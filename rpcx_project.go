package main

import (
	"github.com/smallnest/rpcx/server"
	"rpcx_project/rpcx_server/define_struct"
)

func main() {
	ser := server.NewServer()
	ser.RegisterName("Arith", new(define_struct.Arith), "")
	ser.Serve("tcp", ":8080")
}
