package define_struct

import (
	"encoding/json"
	"golang.org/x/net/context"
	"time"
)

type Args struct {
	A int
	B int
}
type Reply struct {
	C int
}

// 基于已有的实体创建用户自定义类型。
type Arith int

func (t *Arith) Mul(ctx context.Context, args *Args, reply *Reply) error {
	reply.C = args.A * args.B
	return nil
}

func (t *Arith) SendData(ctx context.Context, reponse *ReponseBody) error {
	r := &Request{
		"3123",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		time.Now(),
		1,
		time.Now(),
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
		"3242342",
	}
	marshal, _ := json.Marshal(r)
	reponse = &ReponseBody{"======", string(marshal)}
	return nil
}

type ReponseBody struct {
	msg  string
	data interface{}
}

type Request struct {
	Tenant_code string
	Account     string
	Password    string
	Name        string
	RealName    string
	Email       string
	Phone       string
	Birthday    time.Time
	Sex         int
	LatestTime  time.Time
	RoleId      string
	DeptId      string
	TenantId    string
	Code        string
	Avatar      string
	PostId      string
	CreateDept  string
	UserId      string
}
