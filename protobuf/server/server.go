package server

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net"
	pb "rpcx_project/protobuf/server.helloword"
)

const (
	port = ":8080"
)

type server struct {
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	return &pb.HelloReply{Message: "Hello " + in.Name}, nil
}

func Register() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	s.Serve(lis)
}
