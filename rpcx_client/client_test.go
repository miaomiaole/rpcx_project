package rpcx_client

import (
	"github.com/smallnest/rpcx/client"
	"golang.org/x/net/context"
	"log"
	"rpcx_project/rpcx_server/define_struct"
	"testing"
)

var args = &define_struct.Args{
	A: 10,
	B: 20,
}
var discovery, _ = client.NewPeer2PeerDiscovery("tcp@:8080", "")

// servicePath 服务路径，在Go实现的服务中就是服务端注册的服务名
// 容错模式，这个使用的是同一个服务器多次重试
// RandomSelect 负载均衡模式
// 点对点的服务发现模式 discovery
//DefaultOption 提供一些额外的信息，比如编码模式、TLS信息等。
var xclient = client.NewXClient("Arith", client.Failtry, client.RandomSelect, discovery, client.DefaultOption)

func Test_client(t *testing.T) {

	reply := &define_struct.Reply{}
	// 同步调用
	err2 := xclient.Call(context.Background(), "Mul", args, reply)
	if err2 != nil {
		log.Fatalf("failed to call：%v", err2)
	}
	log.Printf("%d * %d = %d", args.A, args.B, reply.C)

	defer xclient.Close()

}

func Test_asyncClient(t *testing.T) {
	reply := &define_struct.Reply{}
	call, err := xclient.Go(context.Background(), "Mul", args, reply, nil)
	if err != nil {
		log.Fatalf("failed to call: %v", err)
	}
	replyCall := <-call.Done
	if replyCall.Error != nil {
		log.Fatalf("failed to call: %v", replyCall.Error)
	} else {
		log.Printf("%d * %d = %d", args.A, args.B, reply.C)
	}
}

func Test_SendData(t *testing.T) {
	reponse := &define_struct.ReponseBody{}

	err := xclient.Call(context.Background(), "SendData", reponse, nil)
	if err != nil {
		log.Fatalf("failed to call: %v", err)
	}

	log.Printf("返回数据 %v", reponse)
}
